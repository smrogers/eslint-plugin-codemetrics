/**
 * @fileoverview Counts the cyclomatic complexity of each function of the script. See http://en.wikipedia.org/wiki/Cyclomatic_complexity.
 * Counts the number of if, if with else, conditional, for, while, try, switch/case, and returns. Assigning each a configurable score.
 * @author Patrick Brosset, Steven Rogers
 */

'use strict';

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------

module.exports = {
    meta: require('./complexity-meta.js'),
    create: require('./complexity-create.js')
  };
